﻿using Food.Data.Models;

namespace Food.Web.Models
{
    public class HomeViewModel
    {
        public string? Name { get; set; }
        public IEnumerable<Restaurant> data { get; set; }
    }
}
