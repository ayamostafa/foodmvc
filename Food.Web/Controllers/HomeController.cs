﻿using Food.Data.Services;
using Food.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Food.Web.Controllers
{
    public class HomeController : Controller
    {

        IRestaurantData db;
        public HomeController(IRestaurantData db)
        {
            this.db = db;
        }

        public IActionResult Index(string name)
        {
            var model = db.GetAll();

            return View(model);
        }

        public IActionResult Privacy()
        {
            var model = db.GetAll();
            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}