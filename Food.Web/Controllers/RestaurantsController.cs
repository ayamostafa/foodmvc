﻿
using Food.Data.Models;
using Food.Data.Services;
using Microsoft.AspNetCore.Mvc;

namespace Food.Web.Controllers
{
    public class RestaurantsController : Controller
    {
        public readonly IRestaurantData db;
        public RestaurantsController(IRestaurantData db)
        {
            this.db = db;
        }
        public IActionResult Index()
        {
            var model = db.GetAll();
            return View(model);
        }
        public IActionResult Details(int id)
        {

            var model = db.Get(id);
            if (model == null)
            {

                return View("NotFound");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                db.Add(restaurant);

                return RedirectToAction("Details", new { id = restaurant.Id });
            }
            return View();

        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var model = db.Get(id);
            if (model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                db.Update(restaurant);
                return RedirectToAction("Details", new { id = restaurant.Id });
            }
            return View(restaurant);

        }
    }
}
