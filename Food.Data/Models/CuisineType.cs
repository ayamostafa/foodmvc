﻿namespace Food.Data.Models
{
    public enum CuisineType
    {
        None,
        French,
        Italian,
        Egyptian
    }
}
