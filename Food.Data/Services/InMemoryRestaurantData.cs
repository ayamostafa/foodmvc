﻿using Food.Data.Models;

namespace Food.Data.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        List<Restaurant> _restaurants;
        public InMemoryRestaurantData()
        {
            _restaurants = new List<Restaurant>()
                {
                    new Restaurant{Id=1,Name="Cilantro", Cuisine=CuisineType.Egyptian },
                    new Restaurant{Id=2,Name="San", Cuisine=CuisineType.Italian },
                    new Restaurant{Id=3,Name="Zoo", Cuisine=CuisineType.French },
                };
        }
        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurants.OrderBy(r => r.Name);
        }
        public Restaurant Get(int id)
        {
            return _restaurants.FirstOrDefault(r => r.Id == id);
        }
        public void Add(Restaurant restaurant)
        {
            _restaurants.Add(restaurant);
            restaurant.Id = _restaurants.Max(r => r.Id) + 1;
        }
        public void Update(Restaurant restaurant)
        {
            var currentRestaurant = Get(restaurant.Id);
            if (currentRestaurant != null)
            {
                currentRestaurant.Name = restaurant.Name;
                currentRestaurant.Cuisine = restaurant.Cuisine;
            }
        }
    }
}
